ARG PHP_VERSION

FROM pelso/php:${PHP_VERSION}

ARG PHP_VERSION

USER root

RUN apt-get update \
 && apt-get install -y \
    htop \
    vim \
    iputils-ping \
    net-tools \
    mariadb-client \
    sqlite3 \
    postgresql-client \
    mc \
    ranger \
    wget

COPY composer-installer.sh /home/app/composer-installer.sh
RUN /home/app/composer-installer.sh \
    && rm /home/app/composer-installer.sh

RUN wget -O /usr/local/bin/phpunit https://phar.phpunit.de/phpunit-5.phar \
    && chmod +x /usr/local/bin/phpunit

RUN curl -LO https://deployer.org/deployer.phar \
    && mv deployer.phar /usr/local/bin/dep \
    && chmod +x /usr/local/bin/dep

RUN wget -O phpcbf.phar https://squizlabs.github.io/PHP_CodeSniffer/phpcbf.phar \
    && chmod a+x phpcbf.phar \
    && mv phpcbf.phar /usr/local/bin/phpcbf

RUN wget -O phpdcd.phar https://phar.phpunit.de/phpdcd.phar \
    && chmod a+x phpdcd.phar \
    && mv phpdcd.phar /usr/local/bin/phpdcd

RUN wget -O phpcpd.phar https://phar.phpunit.de/phpcpd.phar \
    && chmod a+x phpcpd.phar \
    && mv phpcpd.phar /usr/local/bin/phpcpd

RUN wget -O peridot.phar https://peridot-php.github.io/downloads/peridot.phar \
    && chmod a+x peridot.phar \
    && mv peridot.phar /usr/local/bin/peridot

USER app
WORKDIR /home/app/docroot

ENV PHP_VERSION=${PHP_VERSION}

CMD sudo /usr/sbin/service php${PHP_VERSION}-fpm start \
 && sleep infinity

