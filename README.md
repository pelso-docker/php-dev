# php-dev

## Basics
This project extends my basic php project:
https://hub.docker.com/r/pelso/php or https://gitlab.com/pelso-docker/php

## Php development tools

* composer https://github.com/composer/composer#readme
* phpunit https://phpunit.de/support.html
* deployer https://deployer.org/#what-is-deployer
* phpcbf https://github.com/squizlabs/PHP_CodeSniffer#readme
* phpdcd https://github.com/sebastianbergmann/phpdcd#readme
* phpcpd https://github.com/sebastianbergmann/phpcpd#readme
* peridot https://peridot-php.github.io

## Tools to help around the development process:
* htop https://htop.dev/
* vim https://www.vim.org/about.php
* iputils-ping https://en.wikipedia.org/wiki/Ping_(networking_utility)
* net-tools https://www.linux.co.cr/ldp/lfs/appendixa/net-tools.html
* mariadb-client a mysql client
* sqlite3 an sqlLite client
* postgresql-client a postgres client
* mc https://midnight-commander.org/
* ranger https://github.com/ranger/ranger#readme
* wget https://www.gnu.org/software/wget/

## Php versions & tags, Users & rights, Architectures, Php extensions, Configuration
https://gitlab.com/pelso-docker/php